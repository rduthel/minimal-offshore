json.extract! image, :id, :url, :point_of_sale_id, :created_at, :updated_at
json.url image_url(image, format: :json)
