json.extract! point_of_sale, :id, :name, :city, :updated_at, :client_id, :created_at, :updated_at
json.url point_of_sale_url(point_of_sale, format: :json)
