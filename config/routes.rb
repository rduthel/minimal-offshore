Rails.application.routes.draw do
  resources :images
  resources :point_of_sales
  resources :clients
  get 'welcome/index'

  root 'welcome#index'
end
