class CreatePointOfSales < ActiveRecord::Migration[5.2]
  def change
    create_table :point_of_sales do |t|
      t.string :name
      t.string :city
      t.date :updated_at
      t.references :client, foreign_key: true

      t.timestamps
    end
  end
end
