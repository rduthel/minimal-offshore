class RemoveCreatedAtFromPointOfSale < ActiveRecord::Migration[5.2]
  def change
    remove_column :point_of_sales, :created_at
  end
end
