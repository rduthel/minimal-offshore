def progression(index, total)
  percentage = (((index + 1).to_f / total.to_f) * 100).to_i
  print "#{percentage} %"
  print 13.chr
end

def new_client(index, total)
  progression(index, total)
  Client.create(
            name: Faker::Name.name ,
            language: %w(fr en de).sample,
            active: [true, false].sample
  )
end

def new_point_of_sale(index, total, client_id)
  progression(index, total)
  PointOfSale.create(
                 name: Faker::Restaurant.name,
                 city: [Faker::Address.city, "Paris"].sample,
                 updated_at: Faker::Time.between(4.years.ago, Date.today, :all),
                 client_id: client_id
  )
end

def new_image(index, total)
  progression(index, total)
  Image.create(
      url: Faker::Internet.url,
      point_of_sale_id: @pos_ids.sample
  )
end

puts "Purging DB..."
Image.destroy_all
PointOfSale.destroy_all
Client.destroy_all

puts "Seeding clients"
10.times do |index|
  new_client(index, 10)
end

@client_ids = Client.pluck(:id)
first_client_id = @client_ids.first
last_client_id = @client_ids.last

puts "Seeding point_of_sales for client #{first_client_id}"
1_500.times do |index|
  new_point_of_sale(index, 1_500, first_client_id)
end

puts "Seeding point_of_sales for client #{last_client_id}"
1_001.times do |index|
  new_point_of_sale(index, 1_001, last_client_id)
end

puts "Seeding point_of_sales for random client"
3_000.times do |index|
  new_point_of_sale(index, 3_000, @client_ids.sample)
end

@pos_ids = PointOfSale.pluck(:id)
puts "Seeding images"
10_000.times do |index|
  new_image(index, 10_000)
end

puts "Done."